<?php
require_once('Frog.php');
require_once("Ape.php");
require_once("animal.php");

$sheep = new Animal("Shaun");
echo "Name : " . $sheep->name ."  <br>" ;
echo "Legs : " . $sheep->legs  . " <br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"

$Frog = new Frog("Buduk");
echo " Name : " . $Frog->name . " <br>";
echo " Legs : ". $Frog->legs . " <br>";
echo " Cold Blooded : " . $Frog->cold_blooded . " <br>";
echo $Frog->melompat("Hop Hop") . " <br>";

$Ape = new Ape("Kera Sakti");
echo " Name : " . $Ape->name . " <br>";
echo " Legs : ". $Ape->legs . " <br>";
echo " Cold Blooded : " . $Ape->cold_blooded . " <br>";
echo $Ape->berteriak("Auooo");

