Soal 1 Membuat Database
Buatlah database dengan nama “myshop”. Tulislah di text jawaban pada nomor 1.

CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database
CREATE TABLE `myshop`.`users` (`id` INT(8) NOT NULL , `name` VARCHAR(255) NULL , `email` VARCHAR(255) NOT NULL , `password` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `myshop`.`categories` (`id` INT NOT NULL , `name` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;


CREATE TABLE `myshop`.`items` (`id` INT NOT NULL , `name` VARCHAR(255) NOT NULL , `deskripsi` VARCHAR(255) NOT NULL , `price` INT NOT NULL , `stock` INT NOT NULL , `kategori_id` INT NOT NULL , PRIMARY KEY (`id`, `kategori_id`)) ENGINE = InnoDB;

Soal 3 Memasukkan Data pada Table
Tabel Users

INSERT INTO users (id, name, email, password) VALUES ("001","John Doe", "john@doe.com" , "john123");
INSERT INTO users (id, name, email, password) VALUES ("002","Jane Doe", "jane@doe.com" , "jenita123"); 

Tabel Kategori
INSERT INTO categories (id, name) VALUES ("001" ,"gadget");
INSERT INTO categories (id, name) VALUES ("002" ,"cloth");
INSERT INTO categories (id, name) VALUES ("003" ,"men");
INSERT INTO categories (id, name) VALUES ("004" ,"women");
INSERT INTO categories (id, name) VALUES ("005" ,"branded");

Tabel items
INSERT INTO items (id, name, deskripsi, price, stock, kategori_id) VALUES ("001" ,"Sumsang b50" , "	hape keren dari merek sumsang" , "	4000000" , "100" , "1");

INSERT INTO items (id, name, deskripsi, price, stock, kategori_id) VALUES ("002" ,"Uniklooh" , "baju keren dari brand ternama" , "	500000" , "50" , "2");

INSERT INTO items (id, name, deskripsi, price, stock, kategori_id) VALUES ("003" ,"IMHO Watch" , "	jam tangan anak yang jujur banget" , "2000000" , "10" , "1");

Soal 4 Mengambil Data dari Database

4 a. Mengambil data users
SELECT id, name, deskripsi, price, stock FROM items;

4 b1. Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (Satu Juta).
SELECT * FROM items WHERE price > 1000000;

4 b2 Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM `items` WHERE name LIKE "%uniklo";

4 c. Menampilkan data items join dengan kategori

SELECT items. *, categories.name as genre FROM items INNER JOIN categories ON kategori_id = items.kategori_id;


Soal 5 Mengubah Data dari Database

Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5.
UPDATE items SET stock = 2500000 WHERE id = 4;
